## arista-cvp-api-configlet

Ansible Playbook for using CloudVision Portal API to Review Configuration Compliance


# How To Use

## clone repository

```bash
$ git clone https://gitlab.com/aristacurtis/arista-cvp-config-compliance.git
$ cd arista-cvp-config-compliance
```

## modify relevant variables
  - inventory.ini | `change ansible_host, ansible_user, ansible_password variables`
  - playbook.compliance.yaml | `optionally target specific Container (default "Tenant")`
  - **if** using secrets.yml *_(encrypted vault)_*, create appropriate secrets.yml file and encrypt it:
    ```bash
    $ echo "vault_ansibleuser: \"cvpusername\"" > secrets.yml
    $ echo "vault_ansiblepass: \"cvppassword\"" > secrets.yml
    $ ansible-vault encrypt secrets.yml
    ```

## run it

```bash
ansible-playbook -i inventory.ini --ask-vault-pass playbook.compliance.yaml
```
### what it does

1.  Identifies devices that out of Configuration Compliance
2.  Identifies applied configlets to Out-of-Config-Compliance Devices and crafts json POST Body Content for use with validateAndCompareConfiglets API
3.  Reports new / mismatch / reconcile lines count per Out-of-Config-Compliance Devices
      * TBD: Detail related config block sections; include comparative running-config block sections

## dependencies

This playbook requires the following to be installed on the Ansible control machine:
   -  python 3.7
   -  ansible >= 2.9.0
   -  requests >= 2.22.0
   -  treelib version 1.5.5

      * After installing python3.7+, additional libraries can be installed with:
```bash 
$ pip3 install -r requirements.txt
```
